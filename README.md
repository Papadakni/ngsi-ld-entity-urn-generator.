# Simple NGSI-LD urn generator.

As the name implies, I created this generator using html and javascript as a way to generate urns for NGSI-LD entities based on the NGSI-LD standards and the recommendations of the SynchroniCity project [https://gitlab.com/synchronicity-iot/synchronicity-data-models](url), in order to avoid human mistakes when writing the urns by hand (thus a dropdown list of available data models and domains from [https://smartdatamodels.org/](url) are provided and custom input is supported)  .
This project as of version 0.4, is located entirely in the generator.html file

## INFO

Please note that this tool is in a very early form and thus should be inspected before use. I chose html/css and javascript as an easy way to provide a gui for this tool (since the code is simple) without dealing with pesky gui libraries. 

## Usage
Simply open the generator.html file with a browser of your choice. **Please note  that the browser must support html datalists in order for the comboboxes to work properly**.

## Authors

* **Nikolaos Papadakis csdp1165 [papadakni]**

## Support

* Feel free to contact me at papadakni1996@gmail.com for any questions.

## Roadmap

* TBA
## Acknowledgments

* Hat tip to all the various html guides online that helped me refresh my previous knowledge.
